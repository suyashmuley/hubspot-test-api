Hubspot Test API for Contact and properties of contact

create a test Account using Hubspot Developer Account
Create a Test App and get the API KEY 
create .env file and add API key in dotenv file as API_KEY_TEST = "API KEY HERE"

Endpoints - 
/contact - GET API to get details of all contacts 

/update?email="email address here" - GET API to get all properties using email as query parameter
