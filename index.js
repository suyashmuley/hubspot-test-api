require('dotenv').config();
const express = require("express");
const axios = require("axios");
const bodyParser = require('body-parser');
const PORT = 5000;
const app = express();

app.set('view engine', 'pug');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}))



//API Key for Test App
// const API_KEY_TEST = "6eeb938f-ae20-4059-951f-733eb8150c4a";
const API_KEY_TEST = process.env.API_KEY_TEST;

//API for Get contact data
app.get("/contact", async (req, res) => {
  const contacts = `https://api.hubapi.com/crm/v3/objects/contacts?limit=10&archived=false&hapikey=${API_KEY_TEST}`;
  try {
    const resp = await axios.get(contacts);
    const data = resp.data;
    res.json(data);
  } catch (err) {
    console.log(err);
  }
});


// API to get data (properties) from email as query parameter
app.get('/update', async(req,res)=>{
    const email = req.query.email;
    const getContact = `https://api.hubapi.com/contacts/v1/contact/emails/batch/?email=testingapis@hubspot.com&email=${email}&hapikey=${API_KEY_TEST}`;
    try{
        const response = await axios.get(getContact);
        const data = response.data;
        res.json(data);
    }catch(err){
        console.log(err);
    }
});

app.listen(PORT, () => console.log(`Listening on http://localhost:${PORT}`));
